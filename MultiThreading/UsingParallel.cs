﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class UsingParallel
    {
        public UsingParallel()
        {
            var numbers = Enumerable.Range(0, 20);

            Parallel.For(0, 20, index =>
            {
                Console.WriteLine(numbers.ElementAt(index)+ " - {0}", index);
            });

            Parallel.ForEach(numbers, item =>
            {
                Console.WriteLine("FOREACH - 1 - {0}",item);
            });

            /*
                If you want to stop the loop you have to use Break or Stop options of the ParallelLoopState.
                The Break() ensures that at least tasks that has been intilized will be finalized, but the Stop() no.
            */
            ParallelLoopResult result = Parallel.For(0, 20, (index, loopState) =>
            {

                if(index == 5)
                {
                    Console.WriteLine("Breaking Loop...");
                    //can't use both at the same Parallel loop
                    loopState.Stop();
                    //loopState.Break();
                }
                Console.WriteLine("FOR - 2 - {0}", numbers.ElementAt(index));
            });
            
            /*
                It executes only when all the Parallels for and foreach finishes. They are parallel only 
                    inside the loop.
                If you break or stop the Parallel loop then the IsCompleted will be false, but if runs until
                    the end, then it will be true.
            */
            Console.WriteLine("Loop Finished! {0}", result.IsCompleted);
        }
    }
}
