﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class UsingThreadClass
    {
        /*
            It doesn't matter how you initialize these static fields, since when it goes to the Thread, the Thread will
                consider at the beginning the default value for the types.
            You have to mark the fields as Static, otherwise all threads that use these fields will have access to the same
                value, which may be something that you don't want too and can also cause strange behaviors. Since all
                Threads can modify the same value.
        */
        [ThreadStatic]
        public static int TestStaticParameter = 5;

        [ThreadStatic]
        public static String TestStaticParameter2 = "haha";

        /*
            Creating Fields of this type ensures that you have a intialization for each thread that tries to use
                this Field.
            Different from the use of the [ThreadStatic] annotation, you don't necessarly have to mark these fields as statics.
        */
        public static ThreadLocal<int> TestThreadLocal = new ThreadLocal<int>(() => { return 3; });
        public ThreadLocal<int> TestThreadLocal2 = new ThreadLocal<int>(() => { return 5; });

        public UsingThreadClass()
        {
            Thread threadTest = new Thread(new ThreadStart(ThreadTestMethod));

            Thread threadTest2 = new Thread(new ThreadStart(() =>
            {
                //TestStaticParameter2 = "Thread 2 Modification";
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Thread 2!");

                    TestStaticParameter += 1;
                    Console.WriteLine(TestStaticParameter2 + " - " + TestStaticParameter);
                    Thread.Sleep(1000);
                }
            }));

            Thread threadTest3 = new Thread(new ParameterizedThreadStart(ThreadTestMethodWithParameter));

            Thread threadTest4 = new Thread(new ParameterizedThreadStart((a) =>
            {
                Console.WriteLine("Thread 4 - {0}", a);
            }));

            Thread threadTest5 = new Thread((b) =>
            {

            });

            Thread threadTest6 = new Thread(ThreadTestMethod);

            Thread threadTest7 = new Thread(ThreadTestWithThreadLocal);

            Thread threadTest8 = new Thread(ThreadTestWithThreadLocal2);

            //new Thread(ThreadTestMethodWithParameter).Start("Thread Initialized directly, without Variable"); 

            /*
                If the Thread is marked as Background then as soon as the program finishes executing all of its code
                    the execution is finalized, even if the background Thread hasn't been executed yet. 
                But if the Thread isn't marked as Backgroud, then the code will execute even if the program finishes
                    executing all its code.
            */
            //threadTest2.IsBackground = true;

            Console.WriteLine("Type of Thread: {0}", threadTest2.IsBackground);

            threadTest.Start();
            threadTest2.Start();
            //Used when you have a thread that has a method that is expecting a parameter of type object.
            threadTest3.Start("Test method with parameter");
            threadTest4.Start("Teste Lambda");
            threadTest7.Start();
            threadTest8.Start();

            //threadTest2.Join();
            //threadTest3.Join();

            Console.WriteLine("The ThreadTest2 has finished executing!");
        }

        public void ThreadTestMethod()
        {
            Console.WriteLine("Thread 1!");
        }

        public void ThreadTestMethodWithParameter(object a)
        {
            TestStaticParameter2 = "Thread 3 Modification";
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Thread 3 - {0}", a);

                TestStaticParameter += 2;
                Console.WriteLine(TestStaticParameter2 + " - " + TestStaticParameter);
                Console.WriteLine(TestStaticParameter2);
                Thread.Sleep(1000);
            }
        }

        public void ThreadTestWithThreadLocal()
        {
            TestThreadLocal2.Value = 15;
            for (int i = 0; i < TestThreadLocal2.Value; i++)
            {
                Console.WriteLine("Thread with ThreadLocal 1 - {0}", i);
                Thread.Sleep(500);
            }
        }

        public void ThreadTestWithThreadLocal2()
        {
            TestThreadLocal2.Value = 10;
            for (int i = 0; i < TestThreadLocal2.Value; i++)
            {
                Console.WriteLine("Thread with ThreadLocal 2 - {0}", i);
                Thread.Sleep(500);
            }
        }
    }
}
