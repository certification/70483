﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class UsingPLINQ
    {
        public UsingPLINQ()
        {
            var numbers = Enumerable.Range(0, 100);
            var parallelResult = numbers.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism)
                                 .Where(i => i % 2 == 0).ToList();
            parallelResult.ForEach((item) =>
            {
                Console.WriteLine(item);
            });
        }
    }
}
