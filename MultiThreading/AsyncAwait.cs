﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class AsyncAwait
    {
        public AsyncAwait()
        {
            Console.WriteLine("STARTED!");

            /*
             *  The code runs syncronous when you just use the Async keyword, if you want it to run assyncronous,
             *      you have to use a Task or another Thread.
             *  The asynce keyword is useful for I/O bound operations. 
             */ 
            Task<String> async1 = GetSite();
            Task async2 = Task.Run(() =>
            {
                Console.WriteLine(async1.Result);
            });
            
            Console.WriteLine("Passed the downloading of the Site!");
        }

        public async Task<String> GetSite()
        {
            using (HttpClient client = new HttpClient())
            {
                string result = await client.GetStringAsync("http://www.microsoft.com");
                return result;
            }
        }
    }
}
