﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class ConcurrentCollections
    {
        public ConcurrentCollections()
        {
            BlockingCollection<String> blockingCollection = new BlockingCollection<string>();
            blockingCollection.Add("Test1");
            blockingCollection.Add("Test2");
            Console.WriteLine("START OF BLOCKING COLLECTION!");
            
            //It blocks the execution until a new Item is available.
            /*foreach (var item in blockingCollection.GetConsumingEnumerable())
            {
                Console.WriteLine(item);
            }*/

            //When using the Take() method you the item is removed from the Collection.
            /*while (true)
            {
                Console.WriteLine(blockingCollection.Take());
            }*/

            ConcurrentBag<int> concurrentBag = new ConcurrentBag<int>();
            concurrentBag.Add(23);
            concurrentBag.Add(40);
            Console.WriteLine();
            Console.WriteLine("START OF CONCURRENT BAG!");
            int value = 0;

            //TryTake removes the item from the ConcurrentBag.
            //It always act at the last added value.
            concurrentBag.TryTake(out value);
            Console.WriteLine(value);

            //TryPeek just gets the item from the ConcurrentBag, without removing it.
            concurrentBag.TryPeek(out value);
            Console.WriteLine(value);

            concurrentBag.Add(56);

            concurrentBag.TryTake(out value);
            Console.WriteLine(value);


            ConcurrentStack<int> concurrentStack = new ConcurrentStack<int>();
            concurrentStack.Push(34);
            concurrentStack.Push(59);
            Console.WriteLine();
            Console.WriteLine("START OF CONCURRENT STACK!");

            value = 0;

            //The TryPop() method removes the item from the Stack.
            concurrentStack.TryPop(out value);
            Console.WriteLine(value);

            concurrentStack.TryPop(out value);
            Console.WriteLine(value);

            Console.WriteLine(concurrentStack.Count);


            ConcurrentQueue<int> concurrentQueue = new ConcurrentQueue<int>();
            concurrentQueue.Enqueue(60);
            concurrentQueue.Enqueue(83);
            Console.WriteLine();
            Console.WriteLine("START OF CONCURRENT QUEUE!");

            value = 0;

            //The TryDequeue() method removes the item from the Queue.
            concurrentQueue.TryDequeue(out value);
            Console.WriteLine(value);

            Console.WriteLine(concurrentQueue.Count);


            ConcurrentDictionary<int, string> concurrentDictionary = new ConcurrentDictionary<int, string>();
            concurrentDictionary.AddOrUpdate(1, "Test1", (id, currentValue) => currentValue + " Updated");
            //The key/value already existis, then it's updated based on the KEY.
            concurrentDictionary.AddOrUpdate(1, "Test1", (id, currentValue) => currentValue + " Updated");

            Console.WriteLine();
            Console.WriteLine("START OF CONCURRENT DICTIONARY!");

            Console.WriteLine(concurrentDictionary.GetOrAdd(1, ""));

            /*
             *   This first IF will print on the screen the text "Test2".
             *   But the second IF will print on the screen the text "ITEM EXISTIS"
             */
            if (concurrentDictionary.TryAdd(2, "Test2"))
                Console.WriteLine(concurrentDictionary.GetOrAdd(2, ""));
            else
                Console.WriteLine("ITEM EXISTIS!");

            if (concurrentDictionary.TryAdd(2, "Test2"))
                Console.WriteLine(concurrentDictionary.GetOrAdd(2, ""));
            else
                Console.WriteLine("ITEM EXISTIS!");
        }
    }
}
