﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    public class UsingTPL
    {
        public UsingTPL()
        {
            Task task1 = new Task(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Task 1! - {0}", i);
                    Thread.Sleep(1000);
                }
                Console.WriteLine("Task 1 FINISHED!");
            });
            
            task1.Start();

            //It's the preferred way of executing Tasks.
            Task task2 = Task.Run(() =>
            {
                /*  You can set the Thread as not background, so the program doesn't close when your task finishes
                        executing.
                */
                //Thread.CurrentThread.IsBackground = false;

                for (int i = 0; i < 15; i++)
                {
                    Console.WriteLine("Task 2! - {0}", i);
                    Thread.Sleep(1000);
                }
                Console.WriteLine("Task 2 FINISHED!");
            });

            Task<int> task3 = Task.Run(() =>
            {
                return 1;   
            });

            Task<string> task4 = Task.Run(() => TaskMethodReturnValue());

            /*
                Passing a parameter to a method from a Task is easier than doing with a Thread. 
                You just need to pass the parameter as you do normally with a method.
            */
            Task task5 = Task.Run(() => TaskMethodWithParameters("Test"));

            Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Task started from the Factory.StartNew()");
            });

            Task task6 = Task.Run(() =>
            {
                Console.WriteLine("TASK 6 STARTED!");
                
                /*
                    This configuration for the TaskFactory must be made inside one specific Task, because it
                        can give an error when you try to execut it in the  main Thread.
                */ 
                TaskFactory taskFactoryWithConfiguration = new TaskFactory(TaskCreationOptions.AttachedToParent, TaskContinuationOptions.OnlyOnRanToCompletion);
                taskFactoryWithConfiguration.StartNew(() =>
                {
                    Console.WriteLine("TEST TaskFactory with ConfigurationOptions!");
                });
            });

            Task parentTask = Task.Run(() =>
            {
                Console.WriteLine("PARENT TASK STARTED!");
            });

            Task continueTask = parentTask.ContinueWith(parent =>
            {
                Console.WriteLine("CHILD TASK STARTED! {0}");
               
            });

            

            /*
                If the Result is not ready yet then the program flow will stop here, waiting until the
                    Result is ready.
            */
            Console.WriteLine(task4.Result);
            Console.WriteLine(task3.Result);

            //Waits for this specific Task to be completed before executing the rest of the code
            //task1.Wait();

            //Waits for all the Tasks that are passed in to be completed before executing the rest of the code.
            //Task.WaitAll(new Task[2] {task1, task2 });

            /*Waits for any one of the Tasks that are passed in to be completed before executing the rest of the code.
                If one of the Tasks finishes executing, then it won't wait for the other to finishe executing.
            */
            //Task.WaitAny(new Task[2] {task1, task2 });
            Console.WriteLine("COMEÇO");
            Console.WriteLine(task4.Result);
            Console.WriteLine("FIMMM");
        }

        public String TaskMethodReturnValue()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(500);
            }
            return "Executing a method that returns a value from a TASK!";
        }

        public void TaskMethodWithParameters(String value)
        {
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine("Task 5 With Parameters {0}", value);
                Thread.Sleep(500);
            }
        }
    }
}
