﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            //Uncomment to test each specific lesson.

            //Test With the Thread Class
            //UsingThreadClass threadClass = new UsingThreadClass();

            //Test With the Task Parallel Library (TPL) Class
            //You can trigger a Task and return here to execute the rest of the code.
            //UsingTPL tpl = new UsingTPL();
            //Using the Parallel Class
            //UsingParallel parallel = new UsingParallel();

            //Using the Async and Await Keyword
            //AsyncAwait asyncAwait = new AsyncAwait();

            //Using Parallel Language Integrated Query (PLINQ)
            //UsingPLINQ pLinq = new UsingPLINQ();

            //Using Concurrent Collections 
            //These collections are Thread Safe.
            //ConcurrentCollections concurrentCollections = new ConcurrentCollections();

            Console.ReadLine();
        }
    }
}
